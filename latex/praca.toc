\select@language {polish}
\contentsline {chapter}{\numberline {1}Wst\IeC {\k e}p}{5}
\contentsline {chapter}{\numberline {2}Teoria}{7}
\contentsline {section}{\numberline {2.1}Eksploracja danych (WEKA)}{7}
\contentsline {section}{\numberline {2.2}Systemy wieloagentowe (JADE)}{8}
\contentsline {chapter}{\numberline {3}Implementacja}{11}
\contentsline {section}{\numberline {3.1}Parsowanie danych wej\IeC {\'s}ciowych}{11}
\contentsline {section}{\numberline {3.2}Przechowywanie przetwarzanych danych}{12}
\contentsline {subsection}{\numberline {3.2.1}Struktura bazy danych}{12}
\contentsline {subsection}{\numberline {3.2.2}Biblioteka Hibernate i mapowanie relacyjno-obiektowe}{12}
\contentsline {section}{\numberline {3.3}Aplikacja wieloagentowa}{13}
\contentsline {section}{\numberline {3.4}Graficzny interfejs u\IeC {\.z}ytkownika}{14}
\contentsline {section}{\numberline {3.5}Przygotowanie i analiza danych}{17}
\contentsline {chapter}{\numberline {4}Analiza zebranych danych}{19}
\contentsline {section}{\numberline {4.1}Badanie korelacji pomi\IeC {\k e}dzy parametrami}{19}
\contentsline {section}{\numberline {4.2}Badanie korelacji parametr\IeC {\'o}w w zale\IeC {\.z}no\IeC {\'s}ci od odleg\IeC {\l }o\IeC {\'s}ci mi\IeC {\k e}dzy stacjami}{20}
\contentsline {section}{\numberline {4.3}Predykcja danych}{22}
\contentsline {chapter}{\numberline {5}Podsumowanie}{25}
\contentsline {section}{\numberline {5.1}Mo\IeC {\.z}liwo\IeC {\'s}\IeC {\'c}i rozwoju aplikacji}{26}
\contentsline {chapter}{Bibliografia}{27}
