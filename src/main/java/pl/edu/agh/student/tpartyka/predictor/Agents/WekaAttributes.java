/*
 * Copyright (C) 2014 tomek
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pl.edu.agh.student.tpartyka.predictor.Agents;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import pl.edu.agh.student.tpartyka.predictor.Parameter.Parameter;

/**
 *
 * @author Tomasz Partyka <tpartyka@loken.pl>
 */
public class WekaAttributes implements Serializable {

    private int hourstopredict;
    private Parameter parameter;
    private ArrayList<Parameter> otherparams;

    public WekaAttributes(int h, Parameter p) {
        hourstopredict = h;
        parameter = p;
    }
    
    public WekaAttributes(int h, Parameter p, ArrayList<Parameter> oth){
        hourstopredict = h; 
        parameter = p;
        otherparams = oth;
    }
    
    public int getHourstopredict() {
        return hourstopredict;
    }

    public Parameter getParameter() {
        return parameter;
    }

    public ArrayList<Parameter> getOtherparams() {
        return otherparams;
    }

    public void setOtherparams(ArrayList<Parameter> otherparams) {
        this.otherparams = otherparams;
    }
   
    
    public void setHourstopredict(int hourstopredict) {
        this.hourstopredict = hourstopredict;
    }

    public void setParameter(Parameter parameter) {
        this.parameter = parameter;
    }
}
