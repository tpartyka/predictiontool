/*
 * Copyright (C) 2014 Tomasz Partyka <tpartya@loken.pl>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pl.edu.agh.student.tpartyka.predictor.Behaviours;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import java.io.IOException;
import java.io.Serializable;
import org.apache.log4j.Logger;


/**
 *
 * @author Tomasz Partyka <tpartyka@loken.pl>
 */

public class PredictionSendBehaviour extends OneShotBehaviour {

    private static final Logger LOG = Logger.getLogger(PredictionSendBehaviour.class.getName());

    Agent a;
    ACLMessage msg;
    AID receiver;

    private boolean finished = false;

    public PredictionSendBehaviour(Agent a, Serializable s, AID receiver) {
        this.receiver = receiver;
        this.a = a;
        this.msg = new ACLMessage(ACLMessage.REQUEST);
        
        jade.util.leap.Properties prop = new jade.util.leap.Properties();
        prop.setProperty("DATA", "PREDICTIONS");
        
        try {
            this.msg.setAllUserDefinedParameters(prop);
            this.msg.setContentObject(s);
        } catch (IOException i) {
            LOG.warn("Cannot send this object!");
        }
    }

    @Override
    public void action() {
        msg.addReceiver(receiver);
        a.send(msg);
        finished = true;
        
        LOG.info("Predictions sended!");
    }
    
}
