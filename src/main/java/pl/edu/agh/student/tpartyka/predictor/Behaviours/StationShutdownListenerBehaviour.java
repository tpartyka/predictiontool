/*
 * Copyright (C) 2014 Tomasz Partyka <tpartya@loken.pl>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pl.edu.agh.student.tpartyka.predictor.Behaviours;

import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import org.apache.log4j.Logger;


/**
 *
 * @author Tomasz Partyka <tpartyka@loken.pl>
 */
public class StationShutdownListenerBehaviour extends jade.proto.states.MsgReceiver {

    private final Agent a;
    private boolean done = false;

    private static final Logger LOG = Logger.getLogger(StationShutdownListenerBehaviour.class.getName());

    public StationShutdownListenerBehaviour(Agent a) {
        this.a = a;
    }

    @Override
    public void action() {
        ACLMessage msg = a.receive();

        if (!(msg == null) && msg.getContent().equals("SHUTDOWN")) {
            LOG.info("Shutting down agent: " + a.getName());
            a.doDelete();
            done = true;
            block();
        }
         block(1000);
    }

    @Override
    public boolean done() {
        return done;
    }
}
