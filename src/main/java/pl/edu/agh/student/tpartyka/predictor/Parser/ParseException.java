/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.edu.agh.student.tpartyka.predictor.Parser;

/**
 *
 * @author Tomasz Partyka <tpartyka@loken.pl>
 */

public class ParseException extends Exception {
    public ParseException(String s){
        super(s);
    }
}
