/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.agh.student.tpartyka.predictor.Parser;

import pl.edu.agh.student.tpartyka.predictor.Parameter.Parameter;
import pl.edu.agh.student.tpartyka.predictor.Station.Station;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 *
 * @author Tomasz Partyka <tpartyka@loken.pl>
 */

public class HtmlParser extends Parser {

    private final Properties properties;
    private static final Logger LOG = Logger.getLogger(HtmlParser.class.getName());

    public HtmlParser(Properties prop) {
        super();
        this.properties = prop;
    }

    @Override
    public Station parseData() throws ParseException {
        try {
            Station out = new Station();

            //LOG.info("Connecting to url: " + properties.getProperty("StationUrl"));
            Document doc = Jsoup.connect(properties.getProperty("StationUrl")).timeout(5000).get();

            Element table = doc.select(properties.getProperty("DataSource")).first();

            if (!properties.getProperty("StationNamePos").isEmpty()) {
                String name = doc.select(properties.getProperty("StationNamePos")).first().text();
                name = name.split(",")[0];
                out.setName(name);
            }

            for (int i = Integer.parseInt(properties.getProperty("ParseStartRow")); i < table.select("tr").size(); i++) {
                Element row = table.select("tr").get(i);

                if (row.select("td").first() == null || row.select("td").first().text().equals("Parametr")) {
                    continue;
                }

                String name = row.select(properties.getProperty("ParameterNameCellType")).get(Integer.parseInt(properties.getProperty("ParameterNamePosition")) - 1).text();
                //LOG.info(name.split(",")[0]);

                String unit = row.select(properties.getProperty("UnitNameCellType")).get(Integer.parseInt(properties.getProperty("UnitNamePosition")) - 1).text();
                String norm = row.select(properties.getProperty("NormNameCellType")).get(Integer.parseInt(properties.getProperty("NormNamePosition")) - 1).text();

                Parameter p = new Parameter();
                p.setName(name);
                p.setUnit(unit);
                
//                if(norm != null && !norm.isEmpty()){
//                    LOG.info(norm);
//                    p.setNorm(Double.parseDouble(norm));
//                }
//                    
//                else
//                    p.setNorm(0.0d);
                
                Date today = new Date();

                if (properties.getProperty("DateGeneration").equals("auto")) {
                    today = DateUtils.truncate(today, Calendar.DAY_OF_MONTH);
                } else if (properties.getProperty("DateGeneration").equals("inherit")) {

                } else {
                    throw new ParseException("Unsupported DateGeneration type.");
                }

                for (int j = Integer.parseInt(properties.getProperty("ValuesInitPos")); j < Integer.parseInt(properties.getProperty("ValuesEndPos")); j++) {
                    if (properties.getProperty("DateGeneration").contains("auto")) {
                        Element temp = row.select(properties.getProperty("ValuesCellType")).get(j);

                        today = DateUtils.addHours(today, 1);
                        //LOG.info(today);
                        
                        if (temp.text().isEmpty()) {
                            p.addValue(today, null);
                        } else {
                            p.addValue(today, Float.parseFloat(temp.text()));
                        }
                        

                    }
                }
  
                out.addParameter(p);

            }

//            // test
//            for(Parameter p : out.getParams()){
//                System.out.println("Nazwa parametru: " + p.getName());
//                System.out.println("Jednostka: " + p.getUnit());
////                System.out.println("Wartości:");
////                for(Value v : p.getVars()){
////                    System.out.println(v.getDate() + " " + v.getVar());
////                }
//                System.out.println();
//            }
//            // end test
            return out;

        } catch (IOException e) {
            throw new ParseException(e.getMessage());
        }
    }
    
    @Override
    public Station parseData(Date d) throws ParseException {
        try {
            Station out = new Station();
            
            String url = properties.getProperty("StationUrl") + "&date_input="+ d.getYear() +"-"+ (d.getMonth()+1) +"-"+ (d.getDate());
            
            LOG.info("Connecting to url: " + url);
            Document doc = Jsoup.connect(url).timeout(8000).get();

            Element table = doc.select(properties.getProperty("DataSource")).first();

            if (!properties.getProperty("StationNamePos").isEmpty()) {
                String name = doc.select(properties.getProperty("StationNamePos")).first().text();
                name = name.split(",")[0];
                out.setName(name);
            }

            for (int i = Integer.parseInt(properties.getProperty("ParseStartRow")); i < table.select("tr").size(); i++) {
                Element row = table.select("tr").get(i);

                if (row.select("td").first() == null || row.select("td").first().text().equals("Parametr")) {
                    continue;
                }

                String name = row.select(properties.getProperty("ParameterNameCellType")).get(Integer.parseInt(properties.getProperty("ParameterNamePosition")) - 1).text();
                //LOG.info(name.split(",")[0]);

                String unit = row.select(properties.getProperty("UnitNameCellType")).get(Integer.parseInt(properties.getProperty("UnitNamePosition")) - 1).text();
                String norm = row.select(properties.getProperty("NormNameCellType")).get(Integer.parseInt(properties.getProperty("NormNamePosition")) - 1).text();

                Parameter p = new Parameter();
                p.setName(name);
                p.setUnit(unit);

                Date today = new Date(d.getYear() - 1900,d.getMonth(),d.getDate());

                if (properties.getProperty("DateGeneration").equals("auto")) {
                    today = DateUtils.truncate(today, Calendar.DAY_OF_MONTH);
                } else if (properties.getProperty("DateGeneration").equals("inherit")) {
                    // need to be implemented
                } else {
                    throw new ParseException("Unsupported DateGeneration type.");
                }

                for (int j = Integer.parseInt(properties.getProperty("ValuesInitPos")) - 1; j < Integer.parseInt(properties.getProperty("ValuesEndPos")); j++) {
                    if (properties.getProperty("DateGeneration").contains("auto")) {
                        Element temp = row.select(properties.getProperty("ValuesCellType")).get(j);

                        today = DateUtils.addHours(today, 1);
                        //LOG.info(temp.text() + " - " + today);
                        if (temp.text().isEmpty()) {
                            p.addValue(today, null);
                        } else {
                            p.addValue(today, Float.parseFloat(temp.text()));
                        }
                        
                        
                    }
                }

                out.addParameter(p);

            }

//            // test
//            for(Parameter p : out.getParams()){
//                System.out.println("Nazwa parametru: " + p.getName());
//                System.out.println("Jednostka: " + p.getUnit());
////                System.out.println("Wartości:");
////                for(Value v : p.getVars()){
////                    System.out.println(v.getDate() + " " + v.getVar());
////                }
//                System.out.println();
//            }
//            // end test
            return out;

        } catch (IOException e) {
            throw new ParseException(e.getMessage());
        }
    }
}
