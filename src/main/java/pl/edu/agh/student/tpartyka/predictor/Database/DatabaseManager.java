/*
 * Copyright (C) 2014 Tomasz Partyka
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pl.edu.agh.student.tpartyka.predictor.Database;

import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.SessionFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import pl.edu.agh.student.tpartyka.predictor.Parameter.Parameter;
import pl.edu.agh.student.tpartyka.predictor.Station.Station;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author Tomasz Partyka <tpartyka@loken.pl>
 */
public class DatabaseManager {

    private static final SessionFactory SessionFactory;
    private static final Logger LOG = Logger.getLogger(DatabaseManager.class.getName());

    private DatabaseManager() {

    }

    static {
        try {
            // Create the SessionFactory from standard (hibernate.cfg.xml) 
            // config file.
            SessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
        } catch (HibernateException ex) {
            // Log the exception. 
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return SessionFactory;
    }

    public static void truncateDatabase() {
        try {
            Session temp = DatabaseManager.SessionFactory.openSession();
            temp.beginTransaction();

            temp.createSQLQuery("truncate table Station").executeUpdate();
            temp.createSQLQuery("truncate table Parameter").executeUpdate();
            temp.createSQLQuery("truncate table Value").executeUpdate();

            temp.createSQLQuery("alter table Station AUTO_INCREMENT= 1").executeUpdate();
            temp.createSQLQuery("alter table Parameter AUTO_INCREMENT= 1").executeUpdate();
            temp.createSQLQuery("alter table Value AUTO_INCREMENT= 1").executeUpdate();

            temp.flush();
            temp.close();

            LOG.info("SUCCESS. DATABASE IS EMPTY.");
        } catch (HibernateException h) {
            h.getLocalizedMessage();
        }

    }

    public static void sendStation(Station station) {

        Session session = DatabaseManager.getSessionFactory().openSession();

        try {

            if (station.getName() != null) {

                // Load station from database
                Query query = session.createQuery("from Station where name = :name");
                query.setParameter("name", station.getName());

                List<Station> results = query.list();

                switch (results.size()) {
                    case 0:
                        session.beginTransaction();
                        session.save(station);
                        session.flush();
                        session.getTransaction().commit();
                        LOG.info("SAVED");
                        break;
                    case 1:
                        Station result = results.get(0);

                        Query pquery = session.createQuery("from Parameter where sid = :sid");
                        pquery.setParameter("sid", result.getSid());

//                        session.getTransaction().commit();
                        List<Parameter> params = pquery.list();

                        for (int i = 0; i < params.size(); i++) {
                            for (int j = 0; j < station.getParams().size(); j++) {

                                Parameter temp = station.getParams().get(j);
                                String tempname = temp.getName();
                                String tempname2 = params.get(i).getName();

                                if (tempname.equals(tempname2)) {
                                    Map<Date, Float> v = params.get(i).getVar();

                                    for (Map.Entry m : temp.getVar().entrySet()) {
                                        if (v.containsKey((Date) m.getKey())) {

                                            v.replace((Date) m.getKey(), (Float) m.getValue());

                                        } else {
                                            v.put((Date) m.getKey(), (Float) m.getValue());
                                        }
                                    }

                                    session.beginTransaction();
                                    params.get(i).setVar(v);

                                    session.update(params.get(i));
                                    session.flush();
                                    session.getTransaction().commit();

                                }
                            }
                        }

                        LOG.info("Station: " + station.getName() + " successfuly updated.");
                        break;
                    default:
                        LOG.warn("Database entity count missmatch!");
                }

                session.flush();
            }

        } catch (HibernateException h) {
            LOG.warn(h.getLocalizedMessage());
        } finally {
            session.close();
        }

    }

    public static Station getStation(String name) {
        Session s = DatabaseManager.getSessionFactory().openSession();

        Query qr = s.createQuery("from Station where name = :name");
        qr.setParameter("name", name);
        List<Station> st = (List<Station>) qr.list();

        s.flush();

        if (st.size() != 1) {
            //throw new Exception("Database stations error.");
        }
        
        
        s.close();
        
        return st.get(0);
                    //CurrentStationName.setText(currentStation.getName());

        
    }

}
