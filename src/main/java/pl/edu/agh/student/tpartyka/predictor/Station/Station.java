/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.edu.agh.student.tpartyka.predictor.Station;

import pl.edu.agh.student.tpartyka.predictor.Parameter.Parameter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


/**
 *
 * @author Tomasz Partyka <tpartya@loken.pl>
 */

public class Station implements Serializable {
    
   private long sid;
   
   private String name;
   
   private String URL;
   
   private List<Parameter> params = new ArrayList();
   
   public Station(){
       
   }
     
    public String getName() {
        return name;
    }

    public String getURL() {
        return URL;
    }
     
   
    public void addParameter(Parameter... p){
       for (Parameter temp : p) {
           temp.setStation(this);
           params.add(temp);
       } 
    }   
    
    public void optimize(){
        for(Parameter p: this.params){
            Iterator<? extends Map.Entry<? extends Date, ? extends Float>> iter = p.getVar().entrySet().iterator();
            Map.Entry<? extends Date, ? extends Float> before = iter.next();
            
            while(iter.hasNext()){
                Map.Entry<Date, Float> current = (Map.Entry)iter.next();
                
                try {
                    if(current.getValue().equals(before.getValue())) {
                        iter.remove();
                    } 
                } catch (NullPointerException e){
                    before = current;
                    continue;
                }       
                before = current;
            }
        }
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public void setSid(long sid) {
        this.sid = sid;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public void setParams(List<Parameter> params) {
        this.params = params;
    }

    public long getSid() {
        return sid;
    }

    public List<Parameter> getParams() {
        return params;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 29 * hash + (this.URL != null ? this.URL.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Station other = (Station) obj;
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        if ((this.URL == null) ? (other.URL != null) : !this.URL.equals(other.URL)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return name;
    }

    
   
   
}
