/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.agh.student.tpartyka.predictor.Agents;

import jade.core.AID;
import pl.edu.agh.student.tpartyka.predictor.Behaviours.ParseDataBehaviour;
import pl.edu.agh.student.tpartyka.predictor.Station.Station;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.Envelope;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;
import org.apache.log4j.Logger;
import pl.edu.agh.student.tpartyka.predictor.Behaviours.DataSendBehaviour;
import pl.edu.agh.student.tpartyka.predictor.Behaviours.StationShutdownListenerBehaviour;

/**
 *
 * @author Tomasz Partyka <tpartyka@loken.pl>
 */
public class StationAgent extends Agent {

    private Station station;

    private Properties property;

    private URI DataLocation;
    private Logger LOG = Logger.getLogger(this.getClass().getName());;
    private AID service;

    @Override
    protected void setup() {

        Object[] args = this.getArguments();

        if (args.length != 0) {
            this.property = (Properties) args[0];
        } else {
            LOG.warn("Invalid properties for station!");
        }

        this.station = new Station();
        this.station.setName(this.getAID().getName());

        LOG.info("StationAgent started. " + this.getAID().getName() + " is ready.");

        this.addBehaviour(new ParseDataBehaviour(this, Integer.parseInt(property.getProperty("CheckEvery"))));

        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());

        ServiceDescription sd = new ServiceDescription();
        sd.setType("STATION");
        sd.setName(getAID().getName());

        dfd.addServices(sd);

        try {
            DFService.register(this, dfd);
        } catch (FIPAException f) {
            LOG.warn(f.getLocalizedMessage());
        }

        this.addBehaviour(new StationShutdownListenerBehaviour(this));

        try {
            DFAgentDescription template = new DFAgentDescription();
            ServiceDescription sv = new ServiceDescription();
            sv.setType("SERVICE");
            template.addServices(sv);
            
            DFAgentDescription[] result = DFService.search(this, template);
            
            if (result.length == 1) {
                this.service = result[0].getName();
            } else {
                throw new FIPAException("Unexpected SERVICE agents count...");
            }

        } catch (FIPAException f) {
            LOG.warn(f.getMessage());
        }

    }

    public URI parseLink(String u) {
        try {
            return new URI(u);
        } catch (URISyntaxException e) {
            LOG.warn("URI missmatch.");
        }
        return null;
    }

    public URI getDataLocation() {
        return DataLocation;
    }

    public void setDataLocation(URI DataLocation) {
        this.DataLocation = DataLocation;
    }

    public void setStation(Station s) {
        this.station = s;

        final Station x = s;

        addBehaviour(new OneShotBehaviour(this) {
            @Override
            public void action() {
                ACLMessage msg = new ACLMessage(ACLMessage.PROPOSE);
                try {
                    jade.util.leap.Properties prop = new jade.util.leap.Properties();
                    prop.setProperty("DATA", "DBSYNC");
                    
                    msg.setContentObject(x);
                    msg.addReceiver(service);
                    msg.setAllUserDefinedParameters(prop);
                    getAgent().send(msg);
                } catch (IOException i) {
                    LOG.warn(i.getLocalizedMessage());
                }
            }
        });
    }

    public Properties getProperty() {
        return property;
    }

    @Override
    protected void takeDown() {
        try {
            DFService.deregister(this);
        } catch (FIPAException f) {
            LOG.warn("Problem with deregister Service.");
        }
    }

}
