/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.agh.student.tpartyka.predictor.Parameter;

import java.io.Serializable;
import pl.edu.agh.student.tpartyka.predictor.Station.Station;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Tomasz Partyka <tpartyka@loken.pl>
 */
public class Parameter implements Serializable {

    private long pid;
    private long sid;

    private String name;
    private String unit;
    
    private Double norm = null;

    private Station s;

    private Map var;

    public long getPid() {
        return pid;
    }

    public long getSid() {
        return sid;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }

    public void setSid(long sid) {
        this.sid = sid;
    }

    public String getName() {
        return name;
    }

    public String getUnit() {
        return unit;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station s) {
        this.station = s;
    }
    private Station station;

    public Parameter() {
        this.var = new HashMap<Date, Float>();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void addValue(Date d, Float f) {
        this.var.put(d, f);
    }

    public void removeValue(Date d) {
        var.remove(d);
    }
    
    public void joinValues(Map<Date, Float> m){
        var.putAll(m);
    }
    
    public Map<Date, Float> getVar() {
        return var;
    }

    public void setVar(Map var) {
        this.var = var;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (int) (this.pid ^ (this.pid >>> 32));
        hash = 67 * hash + (int) (this.sid ^ (this.sid >>> 32));
        hash = 67 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }

    public double getNorm() {
        return norm;
    }

    public void setNorm(Double norm) {
        this.norm = norm;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Parameter other = (Parameter) obj;
        if (this.pid != other.pid) {
            return false;
        }
        if (this.sid != other.sid) {
            return false;
        }
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return name;
    }

}
