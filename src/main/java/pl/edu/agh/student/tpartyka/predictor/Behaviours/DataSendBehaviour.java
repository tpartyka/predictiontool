/*
 * Copyright (C) 2014 Tomasz Partyka <tpartya@loken.pl>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pl.edu.agh.student.tpartyka.predictor.Behaviours;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import java.io.IOException;
import java.io.Serializable;
import org.apache.log4j.Logger;


/**
 *
 * @author Tomasz Partyka <tpartya@loken.pl>
 */
public class DataSendBehaviour extends SimpleBehaviour {

    private static final Logger LOG = Logger.getLogger(DataSendBehaviour.class.getName());

    Agent a;
    ACLMessage msg;
    AID receiver;

    private boolean finished = false;

    public DataSendBehaviour(Agent a, Serializable s, AID receiver) {
        this.receiver = receiver;
        this.a = a;
        this.msg = new ACLMessage(ACLMessage.REQUEST);
        try {
            this.msg.setContentObject(s);
        } catch (IOException i) {
            LOG.warn("Cannot send this object!");
        }
    }

    public DataSendBehaviour(Agent a, String content, AID receiver) {
        this.receiver = receiver;
        this.a = a;
        msg = new ACLMessage(ACLMessage.REQUEST);

        msg.setContent(content);  
    }

    public DataSendBehaviour(Agent a) {
        this.a = a;
        this.msg = new ACLMessage(ACLMessage.INFORM);
    }

    @Override
    public void action() {
        msg.addReceiver(receiver);
        a.send(msg);
        finished = true;
        
        LOG.info("Data sended!");
    }

    @Override
    public boolean done() {
        return finished;
    }
}
