/*
 * Copyright (C) 2014 Tomasz Partyka <tpartya@loken.pl>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pl.edu.agh.student.tpartyka.predictor.Behaviours;

import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import java.io.Serializable;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import pl.edu.agh.student.tpartyka.predictor.Agents.ServiceAgent;
import pl.edu.agh.student.tpartyka.predictor.gui.MainWindow;

/**
 *
 * @author Tomasz Partyka <tpartyka@loken.pl>
 */
public class PredictionsReceiveBehaviour extends jade.proto.states.MsgReceiver {

    class Template implements jade.lang.acl.MessageTemplate.MatchExpression {

        @Override
        public boolean match(ACLMessage msg) {

            if (msg.getUserDefinedParameter("DATA") != null) {
                LOG.info("CHECKING..");
                return (msg.getUserDefinedParameter("DATA").equals("PREDICTIONS")) ? true : false;
            }

            return false;
        }
    }
    private final ServiceAgent a;
    private boolean done = false;

    private static final Logger LOG = Logger.getLogger(PredictionsReceiveBehaviour.class.getName());

    public PredictionsReceiveBehaviour(ServiceAgent a) {
        this.a = a;
    }

    @Override
    public void action() {
        ACLMessage msg = a.receive(new MessageTemplate(new Template()));

        if (msg != null) {

            try {
                Serializable s = msg.getContentObject();
                LOG.info("Predictions received!");
                ArrayList<Float> fvars = (ArrayList<Float>) s;

                float[] floatArray = new float[fvars.size()];
                int i = 0;

                for (Float f : fvars) {
                    floatArray[i++] = (f != null ? f : Float.NaN);
                }

                ((MainWindow) a.getGUI()).addPredictions(floatArray);
                done = true;
            } catch (UnreadableException u) {
                block();
            }

        }
        block(1000);
    }

    @Override
    public boolean done() {
        return done;
    }
}
