/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.agh.student.tpartyka.predictor.Parser;

import pl.edu.agh.student.tpartyka.predictor.Station.Station;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.Properties;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


/**
 *
 * @author Tomasz Partyka <tpartyka@loken.pl>
 */
public abstract class Parser {

    public abstract Station parseData() throws ParseException;
    public abstract Station parseData(Date d) throws ParseException;
    
    private static final Logger LOG = Logger.getLogger(Parser.class.getName());

    private enum ParserType {

        xml("Xml"), html("Html");

        private Class ParserName;
        private final String name;

        ParserType(String s) {
            this.name = s;
            try {
                Class temp = Class.forName("pl.edu.agh.student.tpartyka.predictor.Parser." + StringUtils.capitalize(s) + "Parser");

                if (temp.getSuperclass() == pl.edu.agh.student.tpartyka.predictor.Parser.Parser.class) {
                    this.ParserName = temp;
                } else {
                    LOG.warn(s + "Parser class is not a parser!");
                }

            } catch (ClassNotFoundException e) {
                LOG.warn("No such parser found.");
            }
        }

        public Class getParserClass() {
            return ParserName;
        }
    }

    protected Parser() {

    }

    public static Parser getParser(Properties prop) throws ParseException {

        for (ParserType p : ParserType.values()) {
            if (p.name.equalsIgnoreCase(prop.getProperty("DataType"))) {
                try {
                    return (Parser) p.ParserName.getDeclaredConstructor(Properties.class).newInstance(prop);
                } catch (NoSuchMethodException n) {
                    LOG.warn(n.getLocalizedMessage());
                } catch (InstantiationException i) {
                    LOG.warn(i.getLocalizedMessage());
                } catch (IllegalAccessException l) {
                    LOG.warn(l.getLocalizedMessage());
                } catch (InvocationTargetException t) {
                    LOG.warn(t.getLocalizedMessage());
                }
            }
        }
        throw new ParseException("There is no parser for: " + prop.getProperty("DataType"));
    }
}
