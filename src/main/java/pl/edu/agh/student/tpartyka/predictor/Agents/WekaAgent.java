/*
 * Copyright (C) 2014 Tomasz Partyka <tpartyka@loken.pl>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 SELECT v1.`var`, v2.`var`, v1.`date` from `Value` v1, `Value` v2  WHERE v1.pid = (SELECT `pid` from `Parameter` p WHERE p.sid = 1 AND p.name = 'Temperatura (TP)') AND v2.pid = (SELECT `pid` from `Parameter` p WHERE p.sid = 1 AND p.name = 'Prędkość wiatru (WS)') AND v1.date = v2.date;
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pl.edu.agh.student.tpartyka.predictor.Agents;

import jade.core.AID;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import static java.lang.Math.abs;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;
import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;
import pl.edu.agh.student.tpartyka.predictor.Behaviours.DataReceiveBehaviour;
import pl.edu.agh.student.tpartyka.predictor.Parameter.Parameter;
import weka.classifiers.evaluation.NumericPrediction;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.classifiers.timeseries.WekaForecaster;
import weka.classifiers.timeseries.core.TSLagMaker;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.FastVector;
import weka.core.Instances;
import weka.core.Utils;
import weka.core.converters.ArffSaver;

/**
 *
 * @author Tomasz Partyka <tpartyka@loken.pl>
 */
public class WekaAgent extends jade.core.Agent {

    private Instances Training = null;
    private Instances Support = null;

    private int TrainingSteps = 100;
    private int HoursPerStep = 4;

    private WekaForecaster forecaster;

    private static final Logger LOG = Logger.getLogger(WekaAgent.class.getName());
    private AID ServiceAID;

    private FastVector dayofweek_fw;
    private FastVector isweekend_fw;

    private String[] supportArgsNames;

    @Override
    public void setup() {
        ServiceAID = (AID) getArguments()[0];

        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());

        ServiceDescription sd = new ServiceDescription();
        sd.setType("WEKA");
        sd.setName(getAID().getName());

        dfd.addServices(sd);

        try {
            DFService.register(this, dfd);
        } catch (FIPAException f) {
            LOG.warn(f.getLocalizedMessage());
        }

        addBehaviour(new DataReceiveBehaviour(this));

    }

    private String getDayOfWeek(int day) {
        switch (day) {
            case 1:
                return "Sunday";

            case 2:
                return "Monday";

            case 3:
                return "Tuesday";

            case 4:
                return "Wednesday";

            case 5:
                return "Thursday";

            case 6:
                return "Friday";

            case 7:
                return "Saturday";

            default:
                return null;

        }

    }

    public void init(WekaAttributes w) {
        supportArgsNames = new String[w.getOtherparams().size()];

        Attribute date = new Attribute("date", "yyyy-MM-dd HH:mm");
        Attribute var = new Attribute("value");

        dayofweek_fw = new FastVector(7);
        dayofweek_fw.addElement("Monday");
        dayofweek_fw.addElement("Tuesday");
        dayofweek_fw.addElement("Wednesday");
        dayofweek_fw.addElement("Thursday");
        dayofweek_fw.addElement("Friday");
        dayofweek_fw.addElement("Saturday");
        dayofweek_fw.addElement("Sunday");

        Attribute dayofweek = new Attribute("Day of week", dayofweek_fw);

        isweekend_fw = new FastVector(2);
        isweekend_fw.addElement("true");
        isweekend_fw.addElement("false");

        Attribute isweekend = new Attribute("Is weekend?", isweekend_fw);

        FastVector fw = new FastVector();
        fw.addElement(date);
        fw.addElement(isweekend);
        fw.addElement(dayofweek);
        fw.addElement(var);

        int i = 0;

        if (!w.getOtherparams().isEmpty()) {
            for (Parameter param : w.getOtherparams()) {
                fw.add(new Attribute(param.getName().replaceAll(" ", "_")));
                supportArgsNames[i++] = param.getName().replaceAll(" ", "_");
            }

        }

        Training = new Instances("PredictorFile", fw, 0);
        Training.setClassIndex(4);

        Support = new Instances("Test", fw, 0);
        Support.setClassIndex(4);

        buildInstances(w);

    }

    private synchronized void buildInstances(WekaAttributes w) {

        Training.clear();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        TreeMap<Date, Float> map = new TreeMap(w.getParameter().getVar());
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

        Map<Date, Float[]> alldata = new TreeMap<>();  // Lista wartośći parametrów przypisanych do daty            

        Calendar current = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        current.setTime(map.firstKey());
//            Date current = new Date(map.firstKey().getTime());

        Calendar last = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        last.setTime(map.lastKey());
//            Date last = new Date(map.lastKey().getTime());

        //LOG.info("First: " + alldates.getTime().toGMTString());
        //LOG.info("Last: " + map.lastKey().toGMTString());
        //Prepare alldata
        LOG.info("Additional parameters: " + w.getOtherparams().size());

        if (!w.getOtherparams().isEmpty()) {
            while (current.before(last)) {
                //LOG.info(current.getTime().toGMTString());

                Float[] params = new Float[w.getOtherparams().size() + 1];
                int i = 1;

                params[0] = map.get(current.getTime());

                for (Parameter pm : w.getOtherparams()) {
                    if (pm.getVar().containsKey(current.getTime())) {
                        params[i] = (pm.getVar().get(current.getTime()));
                    }

                    i++;
                }

                // obsługa duplikatow
//                    if(alldata.containsKey(current)){
//                        LOG.warn("Duplicate detected.");
//                        alldata.remove(current);
//                    }
                //wpisanie instancji
                alldata.put(current.getTime(), params);

                // inkrementacja daty
                current.add(Calendar.HOUR_OF_DAY, 1);
                //current = new Date(current.getTime() + 3600*1000);
            }
        }

        for (Map.Entry m : alldata.entrySet()) {
            //double[] attValues = new double[Training.numAttributes()];
            double[] attValues = new double[4];
            cal.setTime((Date) m.getKey());

            float[] values = ArrayUtils.toPrimitive((Float[]) m.getValue(), -9999);
            double[] vals_double = new double[values.length];

            for (int i = 0; i < vals_double.length; i++) {
                if (values[i] != -9999) {
                    vals_double[i] = values[i];
                } else {
                    vals_double[i] = Utils.missingValue();
                }
            }

            try {
                //LOG.info(values.size());
                attValues[0] = Training.attribute("date").parseDate(dateFormat.format((Date) m.getKey()));
            } catch (ParseException ex) {
                continue;
            }

            attValues[1] = isweekend_fw.indexOf((cal.get(Calendar.DAY_OF_WEEK) == 1 || cal.get(Calendar.DAY_OF_WEEK) == 7) ? "true" : "false");
            attValues[2] = dayofweek_fw.indexOf(getDayOfWeek(cal.get(Calendar.DAY_OF_WEEK)));
            attValues[3] = vals_double[0];

            double[] out = ArrayUtils.addAll(attValues, Arrays.copyOfRange(vals_double, 1, vals_double.length));

            Training.add(new DenseInstance(1, out));
        }

        splitInstances();

    }

    public void exportInstances(Instances inst, String filename) {
        try {
            LOG.info("Begin saving dump data.");
            ArffSaver saver = new ArffSaver();
            saver.setInstances(inst);
            saver.setFile(new File(filename + ".arff"));
            saver.writeBatch();
            LOG.info("Data saved to " + filename + ".arff");
        } catch (IOException ex) {
            LOG.warn("Problem with saving instances to arff file.");
        }
    }

    private void splitInstances() {
        int t = this.HoursPerStep * this.TrainingSteps;

        for (int i = 0; i < t; i++) {
            Support.add(Training.remove(Training.numInstances() - t + i));
        }

    }

    /**
     *
     * @param pos1 - position of first attribute in arff file
     * @param pos2 - position of second attribute in arff file
     * @param shift - time shift between attributes - 1 means 1 hour
     * @return corelation coefficient
     */
    public double calculateCorrelation(int pos1, int pos2, int shift) {

        double[] vector1 = new double[Training.numInstances()];
        double[] vector2 = new double[Training.numInstances()];

        for (int i = 0; i < Training.numInstances() - shift; i++) {

            if (Double.isNaN(Training.instance(i + shift).value(pos1)) || Double.isNaN(Training.instance(i).value(pos2))) {
                //LOG.info("Catch NaN");
            } else {
                vector1[i] = Training.instance(i + shift).value(pos1);
                vector2[i] = Training.instance(i).value(pos2);
            }

        }

        //LOG.info(Arrays.toString(vector1));
        //LOG.info(Arrays.toString(vector2));
        return Utils.correlation(vector1, vector2, vector1.length);
    }

    public synchronized void train(int h) {

        exportInstances(Training, "dump");

        if (loadForecaster() != null) {

            LOG.info("Loading forecaster...");
            forecaster = loadForecaster();

        } else {

            LOG.info("Begin building model...");

            forecaster = new WekaForecaster();

            try {

                StringBuilder b = new StringBuilder();
                b.append("value");
                b.append(",Prędkość_wiatru_(WS)");
                //b.append(",Ciśnienie_atmosferyczne_(PH)");
                b.append(",Temperatura_(TP)");
                b.append(",Wilgotność_(RH)");
                b.append(",Dwutlenek_azotu_(NO2)");
                b.append(",Tlenek_węgla_(CO)");
                b.append(",Ciśnienie_atmosferyczne_(PH)");
                b.append(",Tlenki_azotu_(NOx)");
                
//                for (String s : this.supportArgsNames) {
//                    b.append(",");
//                    b.append(s);
//                }

                LOG.info(b.toString());
                forecaster.setFieldsToForecast(b.toString());

            } catch (Exception ex) {
                LOG.warn(ex.getLocalizedMessage());
            }

            weka.classifiers.Classifier model;

            model = new MultilayerPerceptron();

            forecaster.setBaseForecaster(model);

            forecaster.getTSLagMaker().setTimeStampField("date");
            forecaster.getTSLagMaker().setMinLag(1);
            forecaster.getTSLagMaker().setMaxLag(5);

            forecaster.getTSLagMaker().setPeriodicity(TSLagMaker.Periodicity.HOURLY);

            forecaster.getTSLagMaker().setAddDayOfWeek(true);

            forecaster.getTSLagMaker().setAddQuarterOfYear(true);

            try {
                forecaster.getTSLagMaker().createTimeLagCrossProducts(Training);
                forecaster.buildForecaster(Training);
                LOG.info("Forecaster builded.");
            } catch (Exception ex) {
                LOG.warn(ex.getLocalizedMessage());
                ex.printStackTrace();
            }

        }

        try {

            exportInstances(Support, "supportt");
            forecaster.primeForecaster(Training);

            for (int i = 0; i < this.TrainingSteps; i++) {
                LOG.info("Step: " + (i + 1));
                List<List<NumericPrediction>> forecast = forecaster.forecast(this.HoursPerStep);
                DecimalFormat df = new DecimalFormat("##.##");
                Double errsum = 0d;

                for (int q = 0; q < this.HoursPerStep; q++) {

                    NumericPrediction predForTarget = forecast.get(q).get(0);
                    Double real = Double.parseDouble(Support.get(q).toString(3));
                    Double pred = predForTarget.predicted();
                    Double error = abs(pred - real) / real;

                    LOG.info("Prediction: " + Support.get(q).stringValue(0) + " - " + df.format(pred) + " " + df.format(real) + " " + df.format(error * 100) + "%");
                    errsum = errsum + error * 100;

                }

                LOG.info("Avg. error: " + df.format(errsum / this.HoursPerStep) + "%");

                System.out.println();

                forecaster.primeForecaster(Training);
                for (int r = 0; r < this.HoursPerStep; r++) {
                    forecaster.primeForecasterIncremental(Support.remove(0));
                }

            }

        } catch (Exception ex) {
            LOG.warn(ex.getLocalizedMessage());
            ex.printStackTrace();
        }

        saveForecaster(forecaster);

        // training data
//        LOG.info("Begin forecasting...");
//        List<List<NumericPrediction>> forecast;
//        try {
//
//            forecast = forecaster.forecast(h);
//
//            ArrayList<Float> out = new ArrayList();
//
//            LOG.info(" --- PREDICTED VALUES --- ");
//
//            for (int i = 0; i < h; i++) {
//                NumericPrediction predForTarget = forecast.get(i).get(0);
//                LOG.info("Prediction at step: " + i + " - " + predForTarget.predicted());
//                out.add((float) predForTarget.predicted());
//            }
//
//            LOG.info(" --- END --- ");
//
//            addBehaviour(new PredictionSendBehaviour(this, out, ServiceAID));
//
//        } catch (Exception ex) {
//            LOG.warn(ex.getLocalizedMessage());
//        }
    }

    private void saveForecaster(Serializable s) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(
                    new FileOutputStream("models/pm10.model"));
            oos.writeObject(s);
            oos.flush();
            oos.close();
        } catch (IOException e) {
            LOG.warn("Cannot save forecaster!");
        }
    }

    private WekaForecaster loadForecaster() {
        try {
            WekaForecaster cls;
            try (ObjectInputStream ois = new ObjectInputStream(
                    new FileInputStream("models/pm10.model"))) {
                cls = (WekaForecaster) ois.readObject();
            }

            return cls;
        } catch (ClassNotFoundException | IOException c) {
            LOG.warn("Cannot load forecaster!");
        }

        return null;
    }

}
