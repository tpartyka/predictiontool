/*
 * Copyright (C) 2014 Tomasz Partyka <tpartya@loken.pl>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pl.edu.agh.student.tpartyka.predictor.Predictor;

import pl.edu.agh.student.tpartyka.predictor.Database.DatabaseManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author Tomasz Partyka <tpartya@loken.pl>
 */

public class Predictor {

    final static String expectedExtension = "xml";

    private static final Map<String, String> options = new HashMap();
    private static final Map<String, Properties> PropertyFiles = new HashMap();
    
    private static final Logger LOG = Logger.getLogger(Predictor.class.getName());

    public static synchronized void listFilesForFolder(final File folder) {
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry);
            } else {
                if (expectedExtension.equals(FilenameUtils.getExtension(fileEntry.getName()))) {
                    try {
                        Properties properties = new Properties();
                        properties.loadFromXML(new FileInputStream(fileEntry));

                        PropertyFiles.put(FilenameUtils.removeExtension(fileEntry.getName()), properties);

                        LOG.info("Added station info: " + fileEntry.getName());
                    } catch (IOException e) {
                        LOG.warn(e.getLocalizedMessage());
                    }

                } else {
                    LOG.warn("File: " + fileEntry.getName() + " has unsupported extension.");
                }
            }
        }
    }

    public static void main(String[] args) {
        
        try {
            prepareArgs(args);
        } catch(IllegalArgumentException i){
            LOG.warn(i.getLocalizedMessage());
            return;
        }
        
        //List<String> program_args = Arrays.asList(args);
        
        if(Predictor.options.containsKey("-t"))
            DatabaseManager.truncateDatabase();
        
        if(Predictor.options.containsKey("-init"))
            Predictor.listFilesForFolder(new File(Predictor.options.get("-init")));        
        
        if(options.containsKey("-join") && options.containsKey("-agent") && !options.containsKey("-init"))
            jade.Boot.main(new String[]{"-port", "1099", "-host", options.get("-join"), "-agents", "Station:pl.edu.agh.student.tpartyka.predictor.Agents.StationAgent", "-container", "Stations", "-gui"});
        else           
            jade.Boot.main(new String[]{"-agents", "Manager:pl.edu.agh.student.tpartyka.predictor.Agents.ServiceAgent"});
    }

    public static Map<String, Properties> getPropertyFiles() {
        return PropertyFiles;
    }

    private static synchronized void prepareArgs(String[] args) {
        List<String> arglist = Arrays.asList(args);
        
        if(arglist.isEmpty()){
            LOG.info("No arguments passed!");
        }
        
        for (int i = 0; i < arglist.size(); i++) {
            if (arglist.get(i).charAt(0) == '-' && arglist.get(i).length() > 1 && arglist.get(i + 1).charAt(0) != '-') {
                options.put(arglist.get(i), arglist.get(i + 1));
            }
        }
    }
}
