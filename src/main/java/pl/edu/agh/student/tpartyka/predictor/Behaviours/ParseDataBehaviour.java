/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.agh.student.tpartyka.predictor.Behaviours;

import pl.edu.agh.student.tpartyka.predictor.Agents.StationAgent;
import pl.edu.agh.student.tpartyka.predictor.Parser.ParseException;
import pl.edu.agh.student.tpartyka.predictor.Parser.Parser;
import pl.edu.agh.student.tpartyka.predictor.Station.Station;
import jade.core.behaviours.TickerBehaviour;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import pl.edu.agh.student.tpartyka.predictor.Database.DatabaseManager;
import pl.edu.agh.student.tpartyka.predictor.Parameter.Parameter;



/**
 *
 * @author Tomasz Partyka <tpartyka@loken.pl>
 * @since 14.06.2014
 */

public class ParseDataBehaviour extends TickerBehaviour {

    private static final Logger LOG = Logger.getLogger(ParseDataBehaviour.class.getName());

    StationAgent a;
    Parser p;

    public ParseDataBehaviour(StationAgent a, long l) {
        super(a, l);
        this.a = a;

        try {
            this.p = Parser.getParser(a.getProperty());
        } catch (ParseException p) {
            LOG.warn(p.getLocalizedMessage());
        }

    }

    @Override
    public void onTick() {
//        try {
//            Station s = p.parseData();
//            //s.optimize();
//            a.setStation(s);
//        } catch (ParseException p) {
//            LOG.warn(p.getMessage());
//        }
        parseAllData();
    }
    
    public void parseAllData() {
        for (int m = 10; m < 12; m++) {
            for (int d = 1; d < 31; d++) {
                try {
                    Station s = p.parseData(new Date(2013, m,d));
                    a.setStation(s);
                    //s.optimize();
                    //DatabaseManager.sendStation(s);
                    sendStation(s);
                } catch (ParseException p) {
                     LOG.warn(p.getMessage());
                }
            }
        }
    }
    
    public static void sendStation(Station station) {

        Session session = DatabaseManager.getSessionFactory().openSession();

        try {

            if (station.getName() != null) {

                // Load station from database
                Query query = session.createQuery("from Station where name = :name");
                query.setParameter("name", station.getName());

                List<Station> results = query.list();

                switch (results.size()) {
                    case 0:
                        session.beginTransaction();
                        session.save(station);
                        session.flush();
                        session.getTransaction().commit();
                        LOG.info("SAVED");
                        break;
                    case 1:
                        Station result = results.get(0);

                        Query pquery = session.createQuery("from Parameter where sid = :sid");
                        pquery.setParameter("sid", result.getSid());

//                        session.getTransaction().commit();
                        List<Parameter> params = pquery.list();

                        for (int i = 0; i < params.size(); i++) {
                            for (int j = 0; j < station.getParams().size(); j++) {

                                Parameter temp = station.getParams().get(j);
                                String tempname = temp.getName();
                                String tempname2 = params.get(i).getName();

                                if (tempname.equals(tempname2)) {
                                    Map<Date, Float> v = params.get(i).getVar();

                                    for (Map.Entry m : temp.getVar().entrySet()) {
                                        if (v.containsKey((Date) m.getKey())) {

                                            v.replace((Date) m.getKey(), (Float) m.getValue());

                                        } else {
                                            v.put((Date) m.getKey(), (Float) m.getValue());
                                        }
                                    }

                                    session.beginTransaction();
                                    params.get(i).setVar(v);

                                    session.update(params.get(i));
                                    session.flush();
                                    session.getTransaction().commit();

                                }
                            }
                        }

                        LOG.info("Station: " + station.getName() + " successfuly updated.");
                        break;
                    default:
                        LOG.warn("Database entity count missmatch!");
                }

                session.flush();
            }

        } catch (HibernateException h) {
            LOG.warn(h.getLocalizedMessage());
        } finally {
            session.close();
        }

    }

    public static Station getStation(String name) {
        Session s = DatabaseManager.getSessionFactory().openSession();

        Query qr = s.createQuery("from Station where name = :name");
        qr.setParameter("name", name);
        List<Station> st = (List<Station>) qr.list();

        s.flush();

        if (st.size() != 1) {
            //throw new Exception("Database stations error.");
        }
        
        
        s.close();
        
        return st.get(0);
                    //CurrentStationName.setText(currentStation.getName());

        
    }


}
