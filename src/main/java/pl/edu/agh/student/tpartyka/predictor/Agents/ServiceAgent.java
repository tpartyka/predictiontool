/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.agh.student.tpartyka.predictor.Agents;

import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.basic.Action;
import pl.edu.agh.student.tpartyka.predictor.Predictor.Predictor;
import jade.core.AID;
import jade.core.Agent;
import jade.wrapper.AgentContainer;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.domain.JADEAgentManagement.JADEManagementOntology;
import jade.domain.JADEAgentManagement.KillAgent;
import jade.domain.JADEAgentManagement.ShutdownPlatform;
import jade.lang.acl.ACLMessage;
import jade.wrapper.AgentController;
import jade.wrapper.PlatformController;
import jade.wrapper.StaleProxyException;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import javax.swing.JFrame;
import pl.edu.agh.student.tpartyka.predictor.gui.MainWindow;
import jade.core.Runtime;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import pl.edu.agh.student.tpartyka.predictor.Behaviours.DataSendBehaviour;
import pl.edu.agh.student.tpartyka.predictor.Behaviours.PredictionsReceiveBehaviour;
import pl.edu.agh.student.tpartyka.predictor.Database.DatabaseManager;
import pl.edu.agh.student.tpartyka.predictor.Parameter.Parameter;
import pl.edu.agh.student.tpartyka.predictor.Station.Station;

/**
 *
 * @author Tomasz Partyka <tpartyka@loken.pl>
 * @since 14.06.2014
 *
 */
public class ServiceAgent extends Agent {

    private Set<AID> StationAgentsAID;
    private AID DatabaseAgentAID;
    private AID WekaAgentAID;

    private JFrame GUI;
    private Profile StationProfile;
    private AgentContainer StationContainer;

    private final PlatformController container = getContainerController();
    private static final Logger LOG = Logger.getLogger(ServiceAgent.class.getName());

    @Override
    protected void setup() {
        Runtime rt = Runtime.instance();
        rt.setCloseVM(true);

        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());

        ServiceDescription sd = new ServiceDescription();
        sd.setType("SERVICE");
        sd.setName(getAID().getName());
        dfd.addServices(sd);

        try {
            DFService.register(this, dfd);
        } catch (FIPAException f) {
            LOG.warn("Cannot register YellowPages service.");
        }

        StationProfile = new ProfileImpl();
        StationProfile.setParameter(Profile.MAIN_HOST, "localhost");
        StationProfile.setParameter(Profile.MAIN_PORT, "1099");
        StationProfile.setParameter(Profile.CONTAINER_NAME, "Stations");

        StationContainer = rt.createAgentContainer(StationProfile);

        LOG.info("ServiceAgent started. " + this.getAID().getName() + " is ready.");

        startStationAgents();

        GUI = new MainWindow(this);
        GUI.setVisible(true);

        addBehaviour(new CyclicBehaviour(this) {

            @Override
            public void action() {
                class Template implements jade.lang.acl.MessageTemplate.MatchExpression {

                    @Override
                    public boolean match(ACLMessage msg) {

                        if (msg.getUserDefinedParameter("DATA") != null) {
                            return (msg.getUserDefinedParameter("DATA").equals("DBSYNC")) ? true : false;
                        }

                        return false;
                    }
                }

                ACLMessage msg = getAgent().receive(new MessageTemplate(new Template()));

                if (msg != null) {
                    switch (msg.getUserDefinedParameter("DATA")) {
                        case "DBSYNC":
                            try {
                                Station s = (Station) msg.getContentObject();
                                DatabaseManager.sendStation(s);
                            } catch (UnreadableException u) {
                                LOG.warn(u.getLocalizedMessage());
                            } finally {
                                break;
                            }
                        case "PREDICTIONS":
                            //addBehaviour(new PredictionsReceiveBehaviour(ServiceAgent.this));
                            break;
                        default:
                            // do nothing
                    }

                }

                block(800);
            }
        });

    }

    public void trainData(Parameter p, ArrayList<Parameter> otherparams, int h) {
        int count = 0;
        int maxTries = 3;

        while (true) {
            try {
                AID weka = findWeka();
                LOG.info(weka.getLocalName());
                addBehaviour(new DataSendBehaviour(this, new WekaAttributes(h,p,otherparams), weka));
                addBehaviour(new PredictionsReceiveBehaviour(this));
                break;
            } catch (FIPAException e) {
                if (count == 0) {
                    LOG.warn("No Weka Agent found. Trying to start...");
                }
                if (count != 0) {
                    LOG.info("Retrying: " + count);
                }

                startWeka();
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException i) {
                    LOG.warn(i.getLocalizedMessage());
                }

                if (++count == maxTries) {
                    LOG.warn("Cannot initialize connection with WEKA agent... breaking");
                    break;
                }
            }
        }

    }

    private AID findWeka() throws FIPAException {

        DFAgentDescription template = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();

        sd.setType("WEKA");
        template.addServices(sd);
        DFAgentDescription[] result = DFService.search(this, template);

        LOG.info("Founded WEKA agents: " + result.length);

        if (result.length == 1) {
            return result[0].getName();
        }

        throw new FIPAException("No Weka Agents.");

    }

    private void startWeka() {
        try {
            LOG.info("Try to start WEKA");
            AgentController temp = getContainerController().createNewAgent("Weka", "pl.edu.agh.student.tpartyka.predictor.Agents.WekaAgent", new Object[]{this.getAID()});
            LOG.info("Starting Weka Agent...");
            temp.start();
            LOG.info("Started. OK");

        } catch (StaleProxyException s) {
            LOG.warn(s.getLocalizedMessage());
        } catch (RuntimeException r) {
            LOG.warn(r.getLocalizedMessage());
        }

    }

    public void addStationAgent(String name, Object[] p) {
        try {
            AgentController temp = StationContainer.createNewAgent(name, "pl.edu.agh.student.tpartyka.predictor.Agents.StationAgent", p);
            temp.start();
        } catch (StaleProxyException s) {
            LOG.warn(s.getLocalizedMessage());
        }
    }

    private synchronized void startStationAgents() {
        if (!Predictor.getPropertyFiles().isEmpty()) {
            for (Entry<String, Properties> single : Predictor.getPropertyFiles().entrySet()) {
                Object[] StationArgs = new Object[]{single.getValue()};
                addStationAgent(single.getKey(), StationArgs);
            }
        } else {
            LOG.warn("There are no agents to run.");
        }
    }

    public void killAgent(AID name) {
        if (this.StationAgentsAID.contains(name)) {
            KillAgent k = new KillAgent();
            k.setAgent(name);

//            try {
//                Action a = new Action();
//                a.setActor(name);
//                a.setAction(k);
//                ACLMessage requestMsg = getRequest();
//            }
            addBehaviour(new DataSendBehaviour(this, "SHUTDOWN", name));

        }
    }

    private void findStationAgents() {
        DFAgentDescription template = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();

        sd.setType("STATION");
        template.addServices(sd);

        Set<AID> out = new HashSet();

        try {
            DFAgentDescription[] result = DFService.search(this, template);

            for (DFAgentDescription d : result) {
                out.add(d.getName());
            }
        } catch (FIPAException f) {
            LOG.warn("Problem with finding Services.");
            LOG.warn(f.getLocalizedMessage());
        }

        StationAgentsAID = out;
    }

    public Set<AID> getStationAgentsAID() {
        findStationAgents();
        return StationAgentsAID;
    }

    public void shutdown() {
        Codec codec = new SLCodec();
        Ontology jmo = JADEManagementOntology.getInstance();
        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(jmo);
        ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
        msg.addReceiver(getAMS());
        msg.setLanguage(codec.getName());
        msg.setOntology(jmo.getName());
        try {
            getContentManager().fillContent(msg, new Action(getAID(), new ShutdownPlatform()));
            send(msg);
        } catch (Exception e) {
            LOG.warn("Problem during shutdown JADE");
        }
    }

    public JFrame getGUI() {
        return GUI;
    }

}
