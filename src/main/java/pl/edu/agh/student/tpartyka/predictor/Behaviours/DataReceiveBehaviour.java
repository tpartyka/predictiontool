/*
 * Copyright (C) 2014 Tomasz Partyka <tpartya@loken.pl>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pl.edu.agh.student.tpartyka.predictor.Behaviours;

import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;
import java.io.Serializable;
import org.apache.log4j.Logger;
import pl.edu.agh.student.tpartyka.predictor.Agents.WekaAgent;
import pl.edu.agh.student.tpartyka.predictor.Agents.WekaAttributes;

/**
 *
 * @author Tomasz Partyka <tpartyka@loken.pl>
 */
public class DataReceiveBehaviour extends jade.proto.states.MsgReceiver {

    private final Agent a;
    private boolean done = false;

    private static final Logger LOG = Logger.getLogger(DataReceiveBehaviour.class.getName());

    public DataReceiveBehaviour(Agent a) {
        
        this.a = a;
    }

    @Override
    public void action() {
        ACLMessage msg = a.receive();

        if (!(msg == null)) {

            try {
                Serializable s = msg.getContentObject();
                LOG.info("Got object!");
                WekaAgent w = (WekaAgent) a;
                
                WekaAttributes atts = (WekaAttributes) s;
                
                w.init(atts);
                w.train(atts.getHourstopredict());

                //done = true;
            } catch (UnreadableException u) {
                block();
            }

            if (msg.getContent().equals("SHUTDOWN")) {
                LOG.info("Shutting down agent: " + a.getName());
                a.doDelete();
            }
       
        }
        
        block(1000);
    }

    
}
